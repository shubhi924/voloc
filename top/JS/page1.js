/*   			Top Page				*/

var flag=0;



$(document).ready(function ()
{
	
    $('.toppage').click( function() {
		$('.bottompage').fadeIn(800);
        $('html,body').animate({ scrollTop: $('.bottompage').offset().top - ( $(window).height() - $('.bottompage').outerHeight(true) ) / 2  }, 500);
		$('.toppage').fadeOut(800);
	});
	
});

/*   			Navbar And Header				*/

function onmenuclick()
{
	if(flag==0)
	{
		$("#navbar").attr("class","animate-navbar");
		$("#opennavbutton").hide();
		var widthnav=$("#navbar").width();
		$("#closenavbutton").css("left",widthnav);
		$("#closenavbutton").show();
		flag=1;
	}
	else
	{
		$("#navbar").attr("class","rev-animate-navbar");
		$("#opennavbutton").show();
		$("#closenavbutton").hide();
		flag=0;
	}
	
}